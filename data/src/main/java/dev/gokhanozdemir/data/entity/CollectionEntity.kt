package dev.gokhanozdemir.data.entity

import com.google.gson.annotations.SerializedName

data class CollectionEntity(
    @SerializedName("id") val id: Int?,
    @SerializedName("title") val title: String?,
    @SerializedName("definition") val definition: String?,
    @SerializedName("start") val start: String?,
    @SerializedName("end") val end: String?,
    @SerializedName("share_url") val share_url: String?,
    @SerializedName("cover") val cover: CoverEntity?,
    @SerializedName("logo") val logo: LogoEntity?
)