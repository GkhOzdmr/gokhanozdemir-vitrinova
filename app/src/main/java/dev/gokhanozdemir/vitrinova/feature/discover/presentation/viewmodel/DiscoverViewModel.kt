package dev.gokhanozdemir.vitrinova.feature.discover.presentation.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import dev.gokhanozdemir.vitrinova.core.extension.isValid
import dev.gokhanozdemir.vitrinova.core.platform.BaseViewModel
import dev.gokhanozdemir.vitrinova.feature.discover.domain.usecase.FetchDiscover
import dev.gokhanozdemir.vitrinova.feature.discover.domain.usecase.FilterDiscoverResponse
import dev.gokhanozdemir.vitrinova.feature.discover.domain.model.FilteredDiscoverModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.PublishSubject
import java.lang.IllegalStateException

class DiscoverViewModel @ViewModelInject constructor(
    private val fetchDiscover: FetchDiscover,
    private val filterDiscoverResponse: FilterDiscoverResponse,
) : BaseViewModel() {

    val fetchAndFilterStateSubject: PublishSubject<FetchAndFilterState> by lazy {
        PublishSubject.create()
    }
    var filteredDiscover: FilteredDiscoverModel? = null

    fun fetchAndFilterDiscover() {
        fetchDiscover.exec().flatMap { discoverResponse ->
            if (discoverResponse.isValid()) {
                discoverResponse.body()?.let {
                    filterDiscoverResponse
                        .exec(FilterDiscoverResponse.Params(it))
                }
            } else {
                Single.error(
                    IllegalStateException(
                        "Fetching and/or filtering \"/discover\" endpoint failed @ DiscoverViewModel::fetchAndFilterDiscover "
                    )
                )
            }
        }
            .prepareObserver(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = { filteredDiscover ->
                    this.filteredDiscover = filteredDiscover
                    this.filteredDiscover?.let {
                        fetchAndFilterStateSubject.onNext(FetchAndFilterState.Success(it))
                    }
                },
                onError = {
                    fetchAndFilterStateSubject.onNext(FetchAndFilterState.Fail(it))
                }
            ).addTo(disposable)
    }

    sealed class FetchAndFilterState {
        data class Success(val filteredDiscoverModel: FilteredDiscoverModel) :
            FetchAndFilterState()

        data class Fail(val t: Throwable?) : FetchAndFilterState()
    }
}