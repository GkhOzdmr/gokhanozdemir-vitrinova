package dev.gokhanozdemir.vitrinova.core.extension

import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import timber.log.Timber


/**
 * Returns last back stack entry, if there is only one entry in the back stack, returns null.
 */
fun Fragment.previousEntryOnBackStack(): FragmentManager.BackStackEntry? {
    return try {
        val fragmentManager = requireActivity().supportFragmentManager
        val backStackLastIndex = fragmentManager.backStackEntryCount - 2
        val backStackEntry = fragmentManager.getBackStackEntryAt(backStackLastIndex)
        /*return*/ backStackEntry
    } catch (iooe: IndexOutOfBoundsException) {
        Timber.e(iooe)
        /*return*/ null
    }
}

fun Fragment.hideSoftInput() {
    activity?.let {
        (it.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager).apply {
            it.currentFocus
            hideSoftInputFromWindow(
                (it.currentFocus ?: View(requireContext())).windowToken,
                0
            )
        }
    }
}

internal fun Fragment.showToast(text: String) {
    Toast.makeText(requireContext(), text, Toast.LENGTH_LONG).show()
}
