package dev.gokhanozdemir.vitrinova.core.extension

/**
 * If referenced object if declared type returns true clause, else false clause.
 */
inline fun <reified T> Any.iftype(its: T.() -> Unit, not: () -> Unit) {
    return if (this is T) {
        its()
    } else {
        not()
    }
}

/**
 * Same as [Any.iftype] but only has true body. False condition does nothing.
 */
inline fun <reified T> Any.iftype(its: T.() -> Unit) {
    if (this is T) {
        its()
    } else {
        return
    }
}
