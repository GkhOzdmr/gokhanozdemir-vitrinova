package dev.gokhanozdemir.data.entity

import com.google.gson.annotations.SerializedName

data class FeaturedEntity(
    @SerializedName("id") val id: Int?,
    @SerializedName("type") val type: String?,
    @SerializedName("cover") val cover: CoverEntity?,
    @SerializedName("title") val title: String?,
    @SerializedName("sub_title") val subTitle: String?
)