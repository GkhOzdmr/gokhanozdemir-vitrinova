package dev.gokhanozdemir.vitrinova.feature.discover.domain.usecase

import dev.gokhanozdemir.data.entity.DiscoverEntity
import dev.gokhanozdemir.data.entity.DiscoverListType
import dev.gokhanozdemir.vitrinova.core.platform.ParameterizedUseCase
import dev.gokhanozdemir.vitrinova.feature.discover.domain.model.*
import io.reactivex.rxjava3.core.Single

class FilterDiscoverResponse :
    ParameterizedUseCase<Single<FilteredDiscoverModel>, FilterDiscoverResponse.Params>() {
    data class Params(val discover: List<DiscoverEntity>)

    override fun exec(params: Params): Single<FilteredDiscoverModel> {
        return Single.just(params.discover).map { discoverResponse ->
            var featured: FeaturedModel? = null
            var newNewProducts: NewProductsModel? = null
            var categories: CategoriesModel? = null
            var collections: CollectionsModel? = null
            var newShops: NewShopsModel? = null
            var editorShops: EditorShopsModel? = null

            discoverResponse.forEachIndexed { index, item ->
                item.type?.let { type ->
                    when (type) {
                        DiscoverListType.FEATURED -> {
                            item.featured?.let {
                                featured = FeaturedModel(item.title, it)
                            }
                        }

                        DiscoverListType.CATEGORIES -> {
                            item.categories?.let {
                                categories = CategoriesModel(item.title, it)
                            }
                        }

                        DiscoverListType.COLLECTIONS -> {
                            item.collections?.let {
                                collections = CollectionsModel(item.title, it)
                            }
                        }

                        DiscoverListType.NEW_PRODUCTS -> {
                            item.products?.let {
                                newNewProducts = NewProductsModel(item.title, it)
                            }
                        }

                        DiscoverListType.NEW_SHOPS -> {
                            item.shops?.let {
                                newShops = NewShopsModel(item.title, it)
                            }
                        }

                        DiscoverListType.EDITOR_SHOPS -> {
                            item.shops?.let {
                                editorShops = EditorShopsModel(item.title, it)
                            }
                        }
                    }
                }
            }

            FilteredDiscoverModel(
                featured,
                newNewProducts,
                categories,
                collections,
                newShops,
                editorShops,
            ) /*^map*/
        }
    }
}