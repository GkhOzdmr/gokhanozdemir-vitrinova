package dev.gokhanozdemir.vitrinova.feature

import android.os.Bundle
import dagger.hilt.android.AndroidEntryPoint
import dev.gokhanozdemir.vitrinova.R
import dev.gokhanozdemir.vitrinova.core.platform.BaseActivity
import dev.gokhanozdemir.vitrinova.core.view.ProgressView
import dev.gokhanozdemir.vitrinova.feature.discover.presentation.fragment.DashboardFragment

@AndroidEntryPoint
class MainActivity : BaseActivity(R.layout.activity_main) {

    override var activityProgressView: ProgressView? = null //TODO set progress view

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportFragmentManager.beginTransaction().apply {
            val fragment = DashboardFragment.newInstance()
            val simpleName = fragment::class.java.simpleName
            replace(R.id.fragment_container, fragment, simpleName)
            addToBackStack(null)
            commit()
        }
    }
}