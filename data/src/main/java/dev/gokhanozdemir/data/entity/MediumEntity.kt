package dev.gokhanozdemir.data.entity

import com.google.gson.annotations.SerializedName

data class MediumEntity(
    @SerializedName("width") val width: Int?,
    @SerializedName("height") val height: Int?,
    @SerializedName("url") val url: String?
)