import org.gradle.api.artifacts.dsl.DependencyHandler

// Only the Versions which is used on multiple different objects should be declared here,
// rest should reside in the related inner [Versions] object.
const val kotlinVersion = "1.4.20"
const val kotlinCoroutines = "1.3.8"
const val daggerVersion = "2.28.1"
const val fragmentVersion = "1.2.5"

object ProjectConfig {
    const val applicationId = "dev.gokhanozdemir.vitrinova"
    const val applicationName = "Vitrinova"
    var versionCode = 1
    var versionName = "1.0.0-alpha1"
}

object BuildPlugins {
    object Version {
        const val daggerHiltGradlePlugin = "2.28-alpha"
    }

    const val daggerHiltGradlePlugin =
        "com.google.dagger:hilt-android-gradle-plugin:${Version.daggerHiltGradlePlugin}"
}

object AndroidSDK {
    const val min = 23
    const val compile = 29
    const val target = compile
    const val buildToolsVersion = "30.0.2"
}

object Dependencies {
    object Module {
        fun DependencyHandler.data() = project(mapOf("path" to ":data"))
    }

    object Kotlin {
        const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib:$kotlinVersion"
        const val kotlinCoroutinesCore =
            "org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinCoroutines"
    }

    object Versions {
        const val timber = "4.7.1"
        const val calligraphy = "3.1.1"
        const val viewpump = "2.0.3"
        const val glide = "4.11.0"
        const val inputMask = "6.0.0"
        const val lottie = "3.4.2"
        const val viewBindingDelegate = "1.2.2"
        const val leakCanary = "2.4"
    }

    const val timber = "com.jakewharton.timber:timber:${Versions.timber}"
    const val calligraphy = "io.github.inflationx:calligraphy3:${Versions.calligraphy}"
    const val viewpump = "io.github.inflationx:viewpump:${Versions.viewpump}"
    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    const val glideOkHttpIntegration =
        "com.github.bumptech.glide:okhttp3-integration:${Versions.glide}"
    const val glideCompiler = "com.github.bumptech.glide:compiler:${Versions.glide}"
    const val inputMask = "com.redmadrobot:input-mask-android:${Versions.inputMask}"
    const val lottie = "com.airbnb.android:lottie:${Versions.lottie}"

    //    const val viewBindingDelegate =
//        "com.kirich1409.viewbindingpropertydelegate:viewbindingpropertydelegate:${Versions.viewBindingDelegate}"
    const val viewBindingDelegateNoReflection =
        "com.kirich1409.viewbindingpropertydelegate:vbpd-noreflection:${Versions.viewBindingDelegate}"
    const val leakCanary = "com.squareup.leakcanary:leakcanary-android:${Versions.leakCanary}"

    object Android {
        object Versions {
            const val androidCore = "1.3.0"
            const val appCompat = "1.2.0"
            const val legacySupport = "1.0.0"
            const val multidex = "2.0.1"
            const val material = "1.2.1"
            const val constraintLayout = "2.0.3"
            const val recyclerView = "1.1.0"
            const val viewPager2 = "1.0.0"
        }

        const val kotlinCoroutinesAndroid =
            "org.jetbrains.kotlinx:kotlinx-coroutines-android:$kotlinCoroutines"
        const val androidCore = "androidx.core:core-ktx:${Versions.androidCore}"
        const val legacySupport = "androidx.legacy:legacy-support-v4:${Versions.legacySupport}"
        const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
        const val multidex = "androidx.multidex:multidex:${Versions.multidex}"
        const val fragment = "androidx.fragment:fragment-ktx:$fragmentVersion"
        const val material = "com.google.android.material:material:${Versions.material}"
        const val constraintLayout =
            "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
        const val recyclerView =
            "androidx.recyclerview:recyclerview:${Versions.recyclerView}"
        const val viewPager2 =
            "androidx.viewpager2:viewpager2:${Versions.viewPager2}"

        object LifeCycle {
            object Versions {
                const val extensions = "2.2.0"
                const val viewModel = "2.2.0"
                const val hiltLifecycle = "1.0.0-alpha01"
            }

            const val extensions = "androidx.lifecycle:lifecycle-extensions:${Versions.extensions}"
            const val viewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.viewModel}"
            const val hiltLifecycleViewModel =
                "androidx.hilt:hilt-lifecycle-viewmodel:${Versions.hiltLifecycle}"
            const val hiltCompiler = "androidx.hilt:hilt-compiler:${Versions.hiltLifecycle}"
        }

        object Test { // androidTestImpl
            object Versions {
                const val androidJunit = "1.1.1"
                const val espressoCore = "3.2.0"
                const val fragmentTesting = fragmentVersion
            }

            const val androidJunit = "androidx.test.ext:junit:${Versions.androidJunit}"
            const val espressoCore = "androidx.test.espresso:espresso-core:${Versions.espressoCore}"
            const val fragmentTesting = "androidx.fragment:fragment-testing:$fragmentVersion"
        }
    }

    object Test {
        object Versions {
            const val junit = "4.12"
            const val kotlinJunit = kotlinVersion
            const val kluent = "1.64"
        }

        const val junit = "junit:junit:${Versions.junit}"
        const val kotlinJunit = "org.jetbrains.kotlin:kotlin-test-junit:${Versions.kotlinJunit}"
        const val kluent = "org.amshove.kluent:kluent-android:${Versions.kluent}"
    }

    object ReactiveExtensions {
        object Versions {
            const val rxJava = "3.0.4"
            const val rxKotlin = "3.0.0"
            const val rxAndroid = "3.0.0"
        }

        const val rxJava = "io.reactivex.rxjava3:rxjava:${Versions.rxJava}"
        const val rxKotlin = "io.reactivex.rxjava3:rxkotlin:${Versions.rxKotlin}"
        const val rxAndroid = "io.reactivex.rxjava3:rxandroid:${Versions.rxAndroid}"
    }

    object Networking {
        object Versions {
            const val gson = "2.8.6"
        }

        const val gson = "com.google.code.gson:gson:${Versions.gson}"

        object Retrofit {
            object Versions {
                const val retrofit = "2.9.0"
                const val rxJava3Adapter = retrofit
                const val gsonConverter = retrofit
            }

            const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
            const val rxJavaAdapter =
                "com.squareup.retrofit2:adapter-rxjava3:${Versions.rxJava3Adapter}"
            const val gsonAdapter =
                "com.squareup.retrofit2:converter-gson:${Versions.gsonConverter}"

        }

        object OkHttp {
            object Versions {
                const val okHttp = "4.7.2"
                const val loggingInterceptor = okHttp
            }

            const val okHttp = "com.squareup.okhttp3:okhttp:${Versions.loggingInterceptor}"
            const val loggingInterceptor =
                "com.squareup.okhttp3:logging-interceptor:${Versions.loggingInterceptor}"
        }
    }

    object Dagger {
        const val daggerHiltVersion = "2.28-alpha"

        object Versions {
            const val dagger = "2.28.1"
            const val daggerAnnotationProcessor = dagger

            // Add normal, androidTest and test implementation
            const val daggerHiltAnnotationProcessor = daggerHiltVersion
        }

        const val dagger = "com.google.dagger:dagger:${Versions.dagger}"
        const val daggerAnnotationProcessor =
            "com.google.dagger:dagger-compiler:${Versions.daggerAnnotationProcessor}"
        const val daggerHilt = "com.google.dagger:hilt-android:${daggerHiltVersion}"

        // Add normal, androidTest and test implementation
        const val daggerHiltAnnotationProcessor =
            "com.google.dagger:hilt-android-compiler:${Versions.daggerHiltAnnotationProcessor}"

        object Test {
            object Versions {
                // Add both androidTest and test implementation
                const val daggerHiltTesting = daggerHiltVersion
            }

            // Add both androidTest and test implementation
            const val daggerHiltTesting =
                "com.google.dagger:hilt-android-testing:${Versions.daggerHiltTesting}"
        }
    }
}
