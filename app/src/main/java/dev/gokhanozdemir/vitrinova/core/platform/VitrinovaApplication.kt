package dev.gokhanozdemir.vitrinova.core.platform

import android.app.Application
import androidx.multidex.MultiDex
import dagger.hilt.android.HiltAndroidApp
import dev.gokhanozdemir.vitrinova.BuildConfig
import timber.log.Timber

@HiltAndroidApp
class VitrinovaApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        plantMultiDex()
        plantTimber()
    }

    private fun plantMultiDex() {
        MultiDex.install(this.applicationContext)
    }

    private fun plantTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Timber.d("Timber has been planted to the project on Debug build!")
        }
    }
}