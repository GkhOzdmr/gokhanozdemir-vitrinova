package dev.gokhanozdemir.vitrinova.feature.discover.presentation.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dev.gokhanozdemir.data.entity.CategoryEntity
import dev.gokhanozdemir.vitrinova.core.extension.iftype
import dev.gokhanozdemir.vitrinova.feature.discover.presentation.adapter.viewholder.CategoriesViewHolder

class CategoriesAdapter(
    val categories: List<CategoryEntity>,
    val listener: CategoriesAdapter.Listener? = null
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun interface Listener {
        fun onItemClick(item: CategoryEntity)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CategoriesViewHolder.onCreate(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.iftype<CategoriesViewHolder> {
            categories.getOrNull(position)?.let { item ->
                bindItem(item)

                listener?.let {
                    holder.itemView.setOnClickListener {
                        listener.onItemClick(item)
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int = categories?.size ?: 0

}