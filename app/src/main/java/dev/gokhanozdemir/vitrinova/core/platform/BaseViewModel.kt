package dev.gokhanozdemir.vitrinova.core.platform

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.Action
import io.reactivex.rxjava3.functions.Consumer
import io.reactivex.rxjava3.subjects.PublishSubject

@Suppress("MemberVisibilityCanBePrivate")
abstract class BaseViewModel : ViewModel() {

    /**
     * Used with [progressStateObservable] for emitting state to show/hide loading indicators.(ie: HUD)
     */
    enum class ProgressState { Show, Hide }

    /**
     * Emits [ProgressState] to determine show/hide state of loading indicators(ie: HUD)
     */
    internal val progressStateObservable: PublishSubject<ProgressState> by lazy {
        PublishSubject.create()
    }

    internal var disposable = CompositeDisposable()

    override fun onCleared() {
        disposeSubscriptions()
        super.onCleared()
    }

    /**
     * Disposes un-disposed subscriptions, should be called at onStop/onDestroy lifecycle state
     */
    internal fun disposeSubscriptions() {
        if (!disposable.isDisposed) disposable.dispose()
    }

    internal fun clearSubscriptions() {
        disposable.clear()
    }

    internal fun emitProgressShow() {
        progressStateObservable.onNext(ProgressState.Show)
    }

    internal fun emitProgressHide() {
        progressStateObservable.onNext(ProgressState.Hide)
    }


    fun <T> Single<T>.prepareObserver(subscribeOnScheduler: Scheduler): Single<T> {
        return this
            .subscribeOn(subscribeOnScheduler)
            .doOnSubscribe(Consumer { emitProgressShow() })
            .doOnTerminate(Action { emitProgressHide() })
    }
}