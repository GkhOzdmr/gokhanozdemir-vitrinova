package dev.gokhanozdemir.data.entity

import com.google.gson.annotations.SerializedName

data class ProductsEntity(
    @SerializedName("id") val id: Int?,
    @SerializedName("code") val code: String?,
    @SerializedName("title") val title: String?,
    @SerializedName("slug") val slug: String?,
    @SerializedName("definition") val definition: String?,
    @SerializedName("old_price") val oldPrice: Int?,
    @SerializedName("price") val price: Int?,
    @SerializedName("stock") val stock: Int?,
    @SerializedName("max_installment") val maxInstallment: String?,
    @SerializedName("commission_rate") val commissionRate: Int?,
    @SerializedName("cargo_time") val cargoTime: Int?,
    @SerializedName("is_cargo_free") val isCargoFree: Boolean?,
    @SerializedName("is_new") val isNew: Boolean?,
    @SerializedName("reject_reason") val rejectReason: String?,
    @SerializedName("category_id") val categoryId: Int?,
    @SerializedName("difference") val difference: String?,
    @SerializedName("is_editor_choice") val isEditorChoice: Boolean?,
    @SerializedName("comment_count") val commentCount: Int?,
    @SerializedName("is_owner") val isOwner: Boolean?,
    @SerializedName("is_approved") val isApproved: Boolean?,
    @SerializedName("is_active") val isActive: Boolean?,
    @SerializedName("share_url") val shareUrl: String?,
    @SerializedName("is_liked") val isLiked: Boolean?,
    @SerializedName("like_count") val likeCount: Int?,
    @SerializedName("shop") val shop: ShopEntity?,
    @SerializedName("category") val category: CategoryEntity?,
    @SerializedName("images") val images: List<ImageEntity>?
)