package dev.gokhanozdemir.vitrinova.core.platform

import androidx.appcompat.app.AppCompatActivity
import dev.gokhanozdemir.vitrinova.core.extension.getLastFragmentOnBackStack
import dev.gokhanozdemir.vitrinova.core.view.ProgressView

abstract class BaseActivity(layoutId: Int) : AppCompatActivity(layoutId) {

    abstract var activityProgressView: ProgressView?

    override fun onBackPressed() {
        // If latest visible fragment allows super.onBackPressed to be called, calls super.
        // Otherwise does not. This also allows fragment to do actions on back press.
        getLastFragmentOnBackStack()?.let { fragment ->
            when (fragment) {
                is BaseFragment -> {
                    if (fragment.backPressAvailable()) {
                        super.onBackPressed()
                    }
                }

                else -> {
                    super.onBackPressed()
                }
            }
        } ?: run {
            super.onBackPressed()
        }
    }
}