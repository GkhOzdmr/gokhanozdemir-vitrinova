package dev.gokhanozdemir.vitrinova.feature.discover

import android.app.SearchManager
import android.content.Intent
import android.os.Bundle
import dev.gokhanozdemir.vitrinova.R
import dev.gokhanozdemir.vitrinova.core.platform.BaseActivity
import dev.gokhanozdemir.vitrinova.core.view.ProgressView

class SearchDiscoverActivity : BaseActivity(R.layout.activity_search_discover) {
    override var activityProgressView: ProgressView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        handleIntent(intent)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.let {
            handleIntent(it)
        }
    }

    private fun handleIntent(intent: Intent) {
        if (Intent.ACTION_SEARCH == intent.action) {
            intent.getStringExtra(SearchManager.QUERY)?.also { query ->
                // Search
            }
        }
    }
}