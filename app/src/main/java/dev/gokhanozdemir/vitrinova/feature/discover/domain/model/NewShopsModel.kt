package dev.gokhanozdemir.vitrinova.feature.discover.domain.model

import dev.gokhanozdemir.data.entity.ShopEntity

data class NewShopsModel(
    val title: String?,
    val list: List<ShopEntity>,
)
