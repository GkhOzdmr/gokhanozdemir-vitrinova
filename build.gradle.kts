// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {
val kotlin_version by extra("1.4.20")
    //    val kotlinVersion by extra("1.4.20")
    repositories {
        google()
        jcenter()
    }

    dependencies {
        classpath("com.android.tools.build:gradle:4.1.1")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
        classpath(BuildPlugins.daggerHiltGradlePlugin)
    }
}

allprojects {
    repositories {
        google()
        jcenter()
    }
}

tasks.register("clean").configure {
    delete("build")
}