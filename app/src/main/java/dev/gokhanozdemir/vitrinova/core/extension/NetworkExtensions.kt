package dev.gokhanozdemir.vitrinova.core.extension

import com.google.gson.Gson
import okhttp3.ResponseBody
import retrofit2.Response


@Suppress("UNCHECKED_CAST")
inline fun <T> Response<T>?.isValid(): Boolean {
    return when {
        this == null -> {
            false
        }

        // 204 has empty body
        this.code() == 204 -> {
            true
        }

        this.body() != null && this.code() in IntRange(200, 400) -> {
            true
        }

        else -> false
    }
}

/**
 * Parses [okhttp3.ResponseBody] to given [T] data class.
 */
inline fun <reified T> parseResponseBody(responseBody: ResponseBody): T =
    Gson().fromJson(
        responseBody.charStream(),
        T::class.java
    )

