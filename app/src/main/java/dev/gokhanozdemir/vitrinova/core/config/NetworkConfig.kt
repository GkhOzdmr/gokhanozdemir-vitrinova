package dev.gokhanozdemir.vitrinova.core.config

import okhttp3.logging.HttpLoggingInterceptor

private const val MILLIS = 1000L
const val DEFAULT_CALL_TIMEOUT_MILLIS = 60.times(MILLIS)
const val DEFAULT_CONNECT_TIMEOUT_MILLIS = 60.times(MILLIS)
const val DEFAULT_READ_TIMEOUT_MILLIS = 60.times(MILLIS)
const val DEFAULT_WRITE_TIMEOUT_MILLIS = 60.times(MILLIS)
val OK_HTTP_LOGGING_LEVEL = HttpLoggingInterceptor.Level.BODY