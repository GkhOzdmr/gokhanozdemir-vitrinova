package dev.gokhanozdemir.vitrinova.feature.discover.presentation.adapter.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dev.gokhanozdemir.data.entity.CollectionEntity
import dev.gokhanozdemir.vitrinova.databinding.ItemCollectionsBinding

class CollectionsViewHolder(
    val binding: ItemCollectionsBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bindItem(item: CollectionEntity) {
    }

    companion object {
        @JvmStatic
        fun onCreate(parent: ViewGroup) =
            CollectionsViewHolder(
                ItemCollectionsBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
    }
}
