package dev.gokhanozdemir.vitrinova.core.di.usecase

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dev.gokhanozdemir.data.VitrinovaService
import dev.gokhanozdemir.vitrinova.feature.discover.domain.usecase.FetchDiscover
import dev.gokhanozdemir.vitrinova.feature.discover.domain.usecase.FilterDiscoverResponse
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object UseCaseModule {

//    @Provides
//    @Singleton
//    fun provide_UseCase(): = ()

    @Provides
    @Singleton
    fun provideFetchDiscoverUseCase(vitrinovaService: VitrinovaService): FetchDiscover =
        FetchDiscover(vitrinovaService)

    @Provides
    @Singleton
    fun provideFilterDiscoverResponseUseCase(): FilterDiscoverResponse =
        FilterDiscoverResponse()
}