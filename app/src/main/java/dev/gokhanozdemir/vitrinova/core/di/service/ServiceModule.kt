package dev.gokhanozdemir.vitrinova.core.di.service

import dev.gokhanozdemir.vitrinova.core.di.network.qualifiers.VitrinovaRetrofit
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dev.gokhanozdemir.data.VitrinovaService
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object ServiceModule {

    @Provides
    @Singleton
    fun provideVitrinovaService(
        @VitrinovaRetrofit vitrinovaRetrofit: Retrofit
    ): VitrinovaService = vitrinovaRetrofit.create(VitrinovaService::class.java)

}