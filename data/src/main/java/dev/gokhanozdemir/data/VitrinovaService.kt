package dev.gokhanozdemir.data

import dev.gokhanozdemir.data.entity.DiscoverEntity
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.GET

interface VitrinovaService {
    object Endpoints {
        object Discover {
            const val mainPath = "api/v2/discover"
        }
    }

    @GET(Endpoints.Discover.mainPath)
    fun fetchDiscover(): Single<Response<List<DiscoverEntity>>>
}