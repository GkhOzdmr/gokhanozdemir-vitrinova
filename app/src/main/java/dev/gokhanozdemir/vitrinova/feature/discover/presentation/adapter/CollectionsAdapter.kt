package dev.gokhanozdemir.vitrinova.feature.discover.presentation.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dev.gokhanozdemir.data.entity.CollectionEntity
import dev.gokhanozdemir.vitrinova.core.extension.iftype
import dev.gokhanozdemir.vitrinova.feature.discover.presentation.adapter.viewholder.CollectionsViewHolder

class CollectionsAdapter(
    val collections: List<CollectionEntity>,
    val listener: CollectionsAdapter.Listener? = null
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    fun interface Listener {
        fun onItemClick(item: CollectionEntity)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CollectionsViewHolder.onCreate(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.iftype<CollectionsViewHolder> {
            collections.getOrNull(position)?.let { item ->
                bindItem(item)

                listener?.let {
                    holder.itemView.setOnClickListener {
                        listener.onItemClick(item)
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int = collections?.size ?: 0
}