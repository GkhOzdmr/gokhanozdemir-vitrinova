package dev.gokhanozdemir.vitrinova.feature.discover.domain.model

data class FilteredDiscoverModel(
    val featured: FeaturedModel?,
    val newProducts: NewProductsModel?,
    val categories: CategoriesModel?,
    val collections: CollectionsModel?,
    val newShops: NewShopsModel?,
    val editorShops: EditorShopsModel?,
)