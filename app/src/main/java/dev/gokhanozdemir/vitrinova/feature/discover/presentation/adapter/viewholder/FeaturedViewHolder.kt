package dev.gokhanozdemir.vitrinova.feature.discover.presentation.adapter.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import dev.gokhanozdemir.data.entity.FeaturedEntity
import dev.gokhanozdemir.vitrinova.databinding.ItemFeaturedBinding

class FeaturedViewHolder(
    private val binding: ItemFeaturedBinding
) : RecyclerView.ViewHolder(binding.root) {

    private val context = binding.root.context

    fun bindItem(item: FeaturedEntity) {
        item.cover?.thumbnail?.url?.let { url ->
            Glide.with(context).load(url).into(binding.imgFeatured)
        }

        item.title?.let { title ->
            binding.tvTitle.text = title
        }

        item.subTitle?.let { subTitle ->
            binding.tvSubtitle.text = subTitle
        }
    }

    companion object {
        @JvmStatic
        fun onCreate(parent: ViewGroup) =
            FeaturedViewHolder(
                ItemFeaturedBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
    }
}
