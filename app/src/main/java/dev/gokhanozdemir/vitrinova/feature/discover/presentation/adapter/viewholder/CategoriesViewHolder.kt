package dev.gokhanozdemir.vitrinova.feature.discover.presentation.adapter.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import dev.gokhanozdemir.data.entity.CategoryEntity
import dev.gokhanozdemir.vitrinova.core.extension.clipViews
import dev.gokhanozdemir.vitrinova.core.extension.onGlobalLayout
import dev.gokhanozdemir.vitrinova.databinding.ItemCategoriesBinding

class CategoriesViewHolder(
    val binding: ItemCategoriesBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bindItem(item: CategoryEntity) {
        item.name?.let { name ->
            binding.tvCategory.text = name
        }

        item.cover?.thumbnail?.url?.let { imgUrl ->
            Glide.with(binding.root.context)
                .load(imgUrl)
                .into(binding.imgCategory)
        }

        binding.root.clipViews()
    }

    companion object {
        @JvmStatic
        fun onCreate(parent: ViewGroup) =
            CategoriesViewHolder(
                ItemCategoriesBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
    }
}
