package dev.gokhanozdemir.data.entity

import com.google.gson.annotations.SerializedName

data class DiscoverEntity(
    @SerializedName("type") val type: DiscoverListType?,
    @SerializedName("title") val title: String?,
    @SerializedName("featured") val featured: List<FeaturedEntity>?,
    @SerializedName("products") val products: List<ProductsEntity>?,
    @SerializedName("categories") val categories: List<CategoryEntity>?,
    @SerializedName("collections") val collections: List<CollectionEntity>?,
    @SerializedName("shops") val shops: List<ShopEntity>?,
)