package dev.gokhanozdemir.vitrinova.feature.discover.domain.model

import dev.gokhanozdemir.data.entity.FeaturedEntity

data class FeaturedModel(
    val title: String?,
    val list: List<FeaturedEntity>,
)
