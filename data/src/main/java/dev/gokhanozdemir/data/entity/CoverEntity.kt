package dev.gokhanozdemir.data.entity

import com.google.gson.annotations.SerializedName

data class CoverEntity(
    @SerializedName("width") val width: Int?,
    @SerializedName("height") val height: Int?,
    @SerializedName("url") val url: String?,
    @SerializedName("medium") val medium: MediumEntity?,
    @SerializedName("thumbnail") val thumbnail: ThumbnailEntity?
)