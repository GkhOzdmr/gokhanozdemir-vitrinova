package dev.gokhanozdemir.vitrinova.feature.discover.domain.model

import dev.gokhanozdemir.data.entity.CategoryEntity

data class CategoriesModel(
    val title: String?,
    val list: List<CategoryEntity>,
)