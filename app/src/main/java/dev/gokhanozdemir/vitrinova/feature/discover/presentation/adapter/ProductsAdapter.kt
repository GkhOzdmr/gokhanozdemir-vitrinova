package dev.gokhanozdemir.vitrinova.feature.discover.presentation.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dev.gokhanozdemir.data.entity.ProductsEntity
import dev.gokhanozdemir.vitrinova.core.extension.iftype
import dev.gokhanozdemir.vitrinova.feature.discover.presentation.adapter.viewholder.NewProductsViewHolder

class ProductsAdapter(
    val products: List<ProductsEntity>,
    val listener: ProductsAdapter.Listener? = null
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun interface Listener {
        fun onItemClick(item: ProductsEntity)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return NewProductsViewHolder.onCreate(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.iftype<NewProductsViewHolder> {
            products.getOrNull(position)?.let { item ->
                bindItem(item)

                listener?.let {
                    holder.itemView.setOnClickListener {
                        listener.onItemClick(item)
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int = products?.size ?: 0
}