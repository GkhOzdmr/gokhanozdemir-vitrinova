package dev.gokhanozdemir.data.entity

import com.google.gson.annotations.SerializedName

enum class DiscoverListType {
    @SerializedName("new_products")
    NEW_PRODUCTS,

    @SerializedName("categories")
    CATEGORIES,

    @SerializedName("featured")
    FEATURED,

    @SerializedName("collections")
    COLLECTIONS,

    @SerializedName("editor_shops")
    EDITOR_SHOPS,

    @SerializedName("new_shops")
    NEW_SHOPS,
}
