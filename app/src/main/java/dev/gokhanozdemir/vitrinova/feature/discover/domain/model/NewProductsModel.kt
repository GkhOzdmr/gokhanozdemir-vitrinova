package dev.gokhanozdemir.vitrinova.feature.discover.domain.model

import dev.gokhanozdemir.data.entity.ProductsEntity

data class NewProductsModel(
    val title: String?,
    val list: List<ProductsEntity>,
)
