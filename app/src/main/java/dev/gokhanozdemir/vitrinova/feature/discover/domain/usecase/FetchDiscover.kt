package dev.gokhanozdemir.vitrinova.feature.discover.domain.usecase

import dev.gokhanozdemir.data.VitrinovaService
import dev.gokhanozdemir.data.entity.DiscoverEntity
import dev.gokhanozdemir.vitrinova.core.platform.UseCase
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import javax.inject.Inject

class FetchDiscover @Inject constructor(
    private val vitrinovaService: VitrinovaService
) : UseCase<Single<Response<List<DiscoverEntity>>>>() {

    override fun exec(): Single<Response<List<DiscoverEntity>>> {
        return vitrinovaService.fetchDiscover()
    }
}