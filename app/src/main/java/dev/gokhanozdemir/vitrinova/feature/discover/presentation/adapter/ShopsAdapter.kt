package dev.gokhanozdemir.vitrinova.feature.discover.presentation.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dev.gokhanozdemir.data.entity.ShopEntity
import dev.gokhanozdemir.vitrinova.feature.discover.domain.model.EditorShopsModel
import dev.gokhanozdemir.vitrinova.feature.discover.domain.model.NewShopsModel
import dev.gokhanozdemir.vitrinova.feature.discover.presentation.adapter.viewholder.EditorShopsViewHolder
import dev.gokhanozdemir.vitrinova.feature.discover.presentation.adapter.viewholder.NewShopsViewHolder

class ShopsAdapter(
    val shopType: ShopType,
    val listener: ShopsAdapter.Listener? = null
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    sealed class ShopType {
        abstract val list: List<ShopEntity>

        class New(val newShopsModel: NewShopsModel) : ShopType() {
            override val list: List<ShopEntity> = newShopsModel.list
        }

        class EditorPick(val editorShopsModel: EditorShopsModel) : ShopType() {
            override val list: List<ShopEntity> = editorShopsModel.list
        }
    }

    fun interface Listener {
        fun onItemClick(shopType: ShopType, item: ShopEntity)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (shopType) {
            is ShopType.EditorPick -> {
                EditorShopsViewHolder.onCreate(parent)
            }

            is ShopType.New -> {
                NewShopsViewHolder.onCreate(parent)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        shopType.list.getOrNull(position)?.let { item ->
            when (holder) {
                is EditorShopsViewHolder -> {
                    holder.bindItem(item)
                }

                is NewShopsViewHolder -> {
                    holder.bindItem(item)
                }
            }

            listener?.let {
                holder.itemView.setOnClickListener {
                    listener.onItemClick(shopType, item)
                }
            }
        }
    }

    override fun getItemCount(): Int = shopType?.list?.size ?: 0
}