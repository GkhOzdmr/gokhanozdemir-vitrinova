package dev.gokhanozdemir.vitrinova.feature.discover.presentation.fragment

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import dev.gokhanozdemir.vitrinova.R
import dev.gokhanozdemir.vitrinova.core.extension.iftype
import dev.gokhanozdemir.vitrinova.core.platform.BaseActivity
import dev.gokhanozdemir.vitrinova.core.platform.BaseFragment
import dev.gokhanozdemir.vitrinova.core.view.ProgressView
import dev.gokhanozdemir.vitrinova.databinding.FragmentDashboardBinding
import dev.gokhanozdemir.vitrinova.feature.discover.domain.model.FilteredDiscoverModel
import dev.gokhanozdemir.vitrinova.feature.discover.presentation.adapter.*
import dev.gokhanozdemir.vitrinova.feature.discover.presentation.viewmodel.DiscoverViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.subjects.PublishSubject
import timber.log.Timber

@AndroidEntryPoint
class DashboardFragment : BaseFragment(R.layout.fragment_dashboard) {

    private var filteredDiscover: FilteredDiscoverModel? = null
    private val binding: FragmentDashboardBinding by viewBinding(FragmentDashboardBinding::bind)
    private val viewModel: DiscoverViewModel by lazy {
        ViewModelProvider(requireActivity()).get(DiscoverViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViews()
    }

    override fun onTransitionAnimFinished(isEnter: Boolean) {
        if (isEnter && isFirstTimeCreation) {
            setProgressAndFetch(false)
        }
    }

    override fun onResume() {
        super.onResume()
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.apply {
            observeFetchAndFilterState(fetchAndFilterStateSubject)
            // tOdo remove?
            if (isFirstTimeCreation) {
            }
        }
    }

    private fun observeFetchAndFilterState(
        fetchAndFilterStateSubject: PublishSubject<DiscoverViewModel.FetchAndFilterState>
    ) {
        fetchAndFilterStateSubject
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { state ->
                    when (state) {
                        is DiscoverViewModel.FetchAndFilterState.Success -> {
                            viewModel.filteredDiscover?.let {
                                this.filteredDiscover = it
                            }
                            setDiscoverViewAdapters()
                        }

                        is DiscoverViewModel.FetchAndFilterState.Fail -> {
                            // TODO Error handling on presenter
                        }
                    }
                },

                onError = {
                    Timber.e("$it @ DashboardFragment::fetchAndFilterStateSubject")
                }
            )
            .addTo(disposable)
    }

    private fun setViews() {
        viewModel.attachProgressObservable()
        setFeaturedRecyclerView()
        setNewProductsRecyclerView()
        setCategoriesRecyclerView()
        setCollectionsRecyclerView()
        setSwipeToRefresh()
        setEditorShopsViewPager()
        setNewShopsViewPager()
    }

    private fun setSwipeToRefresh() {
        binding.srlFragmentDashboard.setOnRefreshListener {
            setProgressAndFetch(true)
        }
    }

    private fun setFeaturedRecyclerView() {
        binding.vp2Featured.apply {
            orientation = ViewPager2.ORIENTATION_HORIZONTAL
            offscreenPageLimit = 1
        }
    }

    private fun setNewProductsRecyclerView() {
        binding.rvProducts.apply {
            layoutManager = LinearLayoutManager(
                requireContext(), LinearLayoutManager.HORIZONTAL, false
            )
        }
    }

    private fun setCategoriesRecyclerView() {
        binding.rvCategories.apply {
            layoutManager = LinearLayoutManager(
                requireContext(), LinearLayoutManager.HORIZONTAL, false
            )
        }
    }

    private fun setCollectionsRecyclerView() {
        binding.rvCollections.apply {
            layoutManager = LinearLayoutManager(
                requireContext(), LinearLayoutManager.HORIZONTAL, false
            )
        }
    }

    private fun setEditorShopsViewPager() {
        binding.vp2EditorShops.apply {
            orientation = ViewPager2.ORIENTATION_HORIZONTAL
            offscreenPageLimit = 3
            setPageTransformer(
                MarginPageTransformer(
                    context.resources.getDimensionPixelSize(
                        R.dimen.spacing_component_1x
                    )
                )
            )
        }
    }

    private fun setNewShopsViewPager() {
        binding.vp2NewShops.apply {
            orientation = ViewPager2.ORIENTATION_HORIZONTAL
            offscreenPageLimit = 3
            setPageTransformer(
                MarginPageTransformer(
                    context.resources.getDimensionPixelSize(
                        R.dimen.spacing_component_1x
                    )
                )
            )
        }
    }

    private fun setDiscoverViewAdapters() {
        filteredDiscover?.let {
            binding.apply {
                it.featured?.let { featured ->
                    vp2Featured.adapter = FeaturedAdapter(featured.list) { _ ->
                    }

                    TabLayoutMediator(tlFeatured, vp2Featured) { tab, position ->
                    }.attach()
                }

                it.newProducts?.let { newProducts ->
                    newProducts.title?.let { title ->
                        tvProductsTitle.text = title
                    }

                    rvProducts.adapter = ProductsAdapter(newProducts.list) { _ ->
                    }
                }

                it.categories?.let { categories ->
                    categories.title?.let { title ->
                        tvCategoriesTitle.text = title
                    }

                    rvCategories.adapter = CategoriesAdapter(categories.list) { _ ->
                    }
                }

                it.collections?.let { collections ->
                    collections.title?.let { title ->
                        tvCollectionsTitle.text = title
                    }

                    rvCollections.adapter = CollectionsAdapter(collections.list) { _ ->
                    }
                }

                it.newShops?.let { newShops ->
                    newShops.title?.let { title ->
                        tvNewShopsTitle.text = title
                    }

                    vp2NewShops.adapter = ShopsAdapter(
                        ShopsAdapter.ShopType.New(newShops)
                    ) { _, _ ->
                    }
                }

                it.editorShops?.let { editorShops ->
                    editorShops.title?.let { title ->
                        tvEditorShopsTitle.text = title
                    }

                    vp2EditorShops.adapter = ShopsAdapter(
                        ShopsAdapter.ShopType.EditorPick(editorShops)
                    ) { _, _ ->
                    }
                }
            }
        }
    }

    private fun setProgressAndFetch(isSwipeRefresh: Boolean) {
        setProgressView(isSwipeRefresh)
        viewModel.fetchAndFilterDiscover()
    }

    /**
     * Sets [ProgressView] of fragment according to fetch method(swipe to refresh or other)
     */
    private fun setProgressView(isSwipeRefresh: Boolean) {
        if (isSwipeRefresh) {
            progressView = object : ProgressView {
                override fun show() {
                }

                override fun hide() {
                    view?.let {
                        binding.srlFragmentDashboard.isRefreshing = false
                    }
                }
            }
        } else {
            activity?.iftype<BaseActivity> {
                progressView = activityProgressView
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = DashboardFragment().apply {
//            arguments = Bundle().apply {
//            }
        }
    }
}