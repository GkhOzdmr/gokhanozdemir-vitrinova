plugins {
    id("com.android.library")
    id("kotlin-android")
}

android {

    compileSdkVersion(AndroidSDK.compile)
    buildToolsVersion = AndroidSDK.buildToolsVersion

    defaultConfig {
        minSdkVersion(AndroidSDK.min)
        targetSdkVersion(AndroidSDK.target)
        versionCode = 1
        versionName = "1.0.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        consumerProguardFile("consumer-rules.pro")
    }


    buildTypes {
        getByName("release") {
            isMinifyEnabled = false

            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
}

dependencies {
    implementation(Dependencies.Kotlin.kotlinStdLib)

    // Networking
    api(Dependencies.Networking.Retrofit.retrofit)
    api(Dependencies.Networking.Retrofit.gsonAdapter)
    api(Dependencies.Networking.Retrofit.rxJavaAdapter)
    api(Dependencies.Networking.gson)
    api(Dependencies.Networking.OkHttp.okHttp)
    api(Dependencies.Networking.OkHttp.loggingInterceptor)

}