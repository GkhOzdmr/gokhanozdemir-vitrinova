package dev.gokhanozdemir.vitrinova.core.di.network

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dev.gokhanozdemir.vitrinova.BuildConfig
import dev.gokhanozdemir.vitrinova.core.config.*
import dev.gokhanozdemir.vitrinova.core.di.network.qualifiers.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object NetworkModule {

    val VITRINOVA_SERVICE_URL: String = BuildConfig.VITRINOVA_SERVICE_URL

    @Singleton
    @Provides
    @VitrinovaRetrofit
    fun provideVitrinovaRetrofit(
        @LenientGSon gSon: Gson,
        @VitrinovaOkHttpClient vitrinovaOkHttpClient: OkHttpClient
    ): Retrofit = Retrofit.Builder().apply {
        baseUrl(VITRINOVA_SERVICE_URL)
        addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        addConverterFactory(GsonConverterFactory.create(gSon))
        client(vitrinovaOkHttpClient)
    }.build()

    @VitrinovaOkHttpClient
    @Provides
    @Singleton
    fun provideVitrinovaOkHttpClient(
        @LoggedOkHttpClientBuilder okHttpClientBuilder: OkHttpClient.Builder,
    ) = okHttpClientBuilder.build()

    @LoggedOkHttpClientBuilder
    @Provides
    @Singleton
    fun provideLoggedOkHttpClientBuilder(
        @DefaultOkHttpClientBuilder okHttpClientBuilder: OkHttpClient.Builder,
    ): OkHttpClient.Builder = okHttpClientBuilder.apply {
        addInterceptor(getHttpLoggingInterceptor())
    }

    private fun getHttpLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().apply {
            level = OK_HTTP_LOGGING_LEVEL
        }
    }

    @Provides
    @DefaultOkHttpClientBuilder
    fun provideDefaultOkHttpBuilder(): OkHttpClient.Builder = OkHttpClient.Builder()
        .callTimeout(DEFAULT_CALL_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)
        .connectTimeout(DEFAULT_CONNECT_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)
        .readTimeout(DEFAULT_READ_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)
        .writeTimeout(DEFAULT_WRITE_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)

    @Provides
    @LenientGSon
    fun provideLenientGson(): Gson = GsonBuilder().setLenient().create()
}