package dev.gokhanozdemir.vitrinova.feature.discover.presentation.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dev.gokhanozdemir.data.entity.FeaturedEntity
import dev.gokhanozdemir.vitrinova.core.extension.iftype
import dev.gokhanozdemir.vitrinova.feature.discover.presentation.adapter.viewholder.FeaturedViewHolder

class FeaturedAdapter(
    val featured: List<FeaturedEntity>,
    val listener: FeaturedAdapter.Listener? = null
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun interface Listener {
        fun onItemClick(item: FeaturedEntity)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return FeaturedViewHolder.onCreate(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.iftype<FeaturedViewHolder> {
            featured.getOrNull(position)?.let { item ->
                bindItem(item)
                listener?.let {
                    holder.itemView.setOnClickListener {
                        listener.onItemClick(item)
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int = featured?.size ?: 0
}