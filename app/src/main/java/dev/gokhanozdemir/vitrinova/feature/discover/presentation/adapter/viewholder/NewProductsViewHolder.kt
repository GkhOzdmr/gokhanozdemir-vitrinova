package dev.gokhanozdemir.vitrinova.feature.discover.presentation.adapter.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import dev.gokhanozdemir.data.entity.ProductsEntity
import dev.gokhanozdemir.vitrinova.core.extension.clipViews
import dev.gokhanozdemir.vitrinova.databinding.ItemNewProductsBinding

class NewProductsViewHolder(
    val binding: ItemNewProductsBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bindItem(item: ProductsEntity) {
        item.images?.getOrNull(0)?.thumbnail?.url?.let { imgUrl ->
            Glide.with(binding.root.context).load(imgUrl).into(binding.imgProduct)
        }

        item.title?.let { title ->
            binding.tvProductName.text = title
        }

        item.shop?.name?.let { shopName ->
            binding.tvBrand.text = shopName
        }

        item.price?.let { price ->
            binding.tvPrice.text = price.toString()
        }

        item.oldPrice?.let { oldPrice ->
            binding.tvOldPrice.text = oldPrice.toString()
        }

        binding.root.clipViews()
    }

    companion object {
        @JvmStatic
        fun onCreate(parent: ViewGroup) =
            NewProductsViewHolder(
                ItemNewProductsBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
    }
}
