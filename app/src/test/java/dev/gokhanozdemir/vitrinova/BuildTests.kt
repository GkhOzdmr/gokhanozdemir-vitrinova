package dev.gokhanozdemir.vitrinova

import org.junit.Assert
import org.junit.Test

class BuildTests {
    @Test
    fun onBuild_Service_Interface_Does_Exist() {
        try {
            Class.forName("dev.gokhanozdemir.data.VitrinovaService")
        } catch (e: ClassNotFoundException) {
            Assert.fail("VitrinovaService does not exist")
        }
    }
}