package dev.gokhanozdemir.vitrinova.feature.discover.presentation.adapter.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dev.gokhanozdemir.data.entity.ShopEntity
import dev.gokhanozdemir.vitrinova.databinding.ItemEditorShopsBinding

class EditorShopsViewHolder(
    private val binding: ItemEditorShopsBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bindItem(item: ShopEntity) {
    }

    companion object {
        @JvmStatic
        fun onCreate(parent: ViewGroup) =
            EditorShopsViewHolder(
                ItemEditorShopsBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
    }
}

//: RecyclerView.ViewHolder(binding.root) {
//
//    companion object {
//        @JvmStatic
//        fun onCreate(parent: ViewGroup) =
//            (
//                .inflate(
//                    LayoutInflater.from(parent.context),
//                    parent,
//                    false
//                )
//        )
//    }
//}