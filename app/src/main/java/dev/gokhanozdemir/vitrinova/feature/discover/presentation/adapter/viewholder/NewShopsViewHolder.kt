package dev.gokhanozdemir.vitrinova.feature.discover.presentation.adapter.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dev.gokhanozdemir.data.entity.ShopEntity
import dev.gokhanozdemir.vitrinova.databinding.ItemNewShopsBinding

class NewShopsViewHolder(
    private val binding: ItemNewShopsBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bindItem(item: ShopEntity) {
    }

    companion object {
        @JvmStatic
        fun onCreate(parent: ViewGroup) =
            NewShopsViewHolder(
                ItemNewShopsBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
    }
}