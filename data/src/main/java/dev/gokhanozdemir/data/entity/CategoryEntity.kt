package dev.gokhanozdemir.data.entity

import com.google.gson.annotations.SerializedName

data class CategoryEntity(
    @SerializedName("id") val id: Int?,
    @SerializedName("name") val name: String?,
    @SerializedName("parent_id") val parentId: String?,
    @SerializedName("order") val order: Int?,
    @SerializedName("parent_category") val parentCategory: CategoryEntity?,
    @SerializedName("logo") val logo: LogoEntity?,
    @SerializedName("cover") val cover: CoverEntity?,
    @SerializedName("children") val children: List<CategoryEntity>?
)