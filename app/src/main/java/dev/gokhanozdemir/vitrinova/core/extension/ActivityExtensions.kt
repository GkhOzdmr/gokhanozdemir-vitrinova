package dev.gokhanozdemir.vitrinova.core.extension

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

fun AppCompatActivity.getLastFragmentOnBackStack(): Fragment? {
    val lastEntryIndex = supportFragmentManager.backStackEntryCount - 1
    val fragmentTag = supportFragmentManager.getBackStackEntryAt(lastEntryIndex).name
    return supportFragmentManager.findFragmentByTag(fragmentTag)
}
