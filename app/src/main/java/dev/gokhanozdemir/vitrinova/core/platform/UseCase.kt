package dev.gokhanozdemir.vitrinova.core.platform

/**
 * Base Use Case
 *
 * @property T Execution return type
 */
abstract class UseCase<T> {
    abstract fun exec(): T
}

/**
 * Parameterized Use Case
 *
 * @property P Execution parameters
 * @property T Execution return type
 */
abstract class ParameterizedUseCase<T, P> {
    abstract fun exec(params: P): T
}
