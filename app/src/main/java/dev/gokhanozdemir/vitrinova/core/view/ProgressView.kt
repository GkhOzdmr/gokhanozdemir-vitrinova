package dev.gokhanozdemir.vitrinova.core.view

/**
 * Will have custom implementation of showing/hiding progress indicator(HUD, native etc.)
 */
interface ProgressView {
    fun show()
    fun hide()
}