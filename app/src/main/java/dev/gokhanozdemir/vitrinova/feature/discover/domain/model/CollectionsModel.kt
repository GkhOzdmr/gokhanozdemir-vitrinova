package dev.gokhanozdemir.vitrinova.feature.discover.domain.model

import dev.gokhanozdemir.data.entity.CollectionEntity

data class CollectionsModel(
    val title: String?,
    val list: List<CollectionEntity>,
)
