package dev.gokhanozdemir.vitrinova.core.platform

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import dev.gokhanozdemir.vitrinova.core.extension.*
import dev.gokhanozdemir.vitrinova.core.view.ProgressView
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.PublishSubject
import timber.log.Timber

abstract class BaseFragment constructor(@LayoutRes private val layoutResId: Int) :
    Fragment(layoutResId) {

    internal val disposable = CompositeDisposable()
    internal var isFirstTimeCreation = true
    internal var progressView: ProgressView? = null

    /**
     * Handles fragment specific onBackPressed actions. If no action to be done
     * by fragment, method will return true and allow activity to call super.onBackPressed
     *
     * True by default
     *
     * @return allow super.onBackPressed to be called. Return true to allow.
     */
    open fun backPressAvailable(): Boolean {
        return true
    }

    open fun onTransitionAnimFinished(isEnter: Boolean) = Unit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        hideSoftInput()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupProgressView()
    }

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return if (nextAnim > 0) {
            val animation = AnimationUtils.loadAnimation(requireActivity(), nextAnim)
            animation?.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation?) {
                }

                override fun onAnimationEnd(animation: Animation?) {
                    onTransitionAnimFinished(enter)
                    animation?.setAnimationListener(null)
                }

                override fun onAnimationRepeat(animation: Animation?) {
                }
            })

            animation
        } else {
            onTransitionAnimFinished(enter)
            super.onCreateAnimation(transit, enter, nextAnim)
        }
    }

    override fun onStop() {
        clearSubscriptions()
        progressView?.hide()
        super.onStop()
    }

    override fun onDestroyView() {
        isFirstTimeCreation = false
        super.onDestroyView()
    }

    private fun setupProgressView() {
        requireActivity().iftype<BaseActivity> {
            activityProgressView?.let {
                progressView = it
            }
        }
    }

    internal fun disposeSubscriptions() {
        if (!disposable.isDisposed) disposable.dispose()
    }

    internal fun clearSubscriptions() {
        disposable.clear()
    }

    /**
     * Will be called after [ProgressView.show]
     * No-opt by default. For custom behaviour, override on child.
     */
    open fun onShowProgress() = Unit /* no-opt by default */

    /**
     * Will be called after [ProgressView.hide]
     * No-opt by default. For custom behaviour, override on child.
     */
    open fun onHideProgress() = Unit /* no-opt by default */

    internal fun showHud() {
        progressView?.show()
    }

    internal fun hideHud() {
        progressView?.hide()
    }

    internal fun BaseViewModel.attachProgressObservable() {
        observeProgressState(this.progressStateObservable)
    }

    /**
     * Will show/hide [ProgressView] according to [BaseViewModel.ProgressState]
     *
     * !!NOTICE!! Should set [progressView] with implementation
     */
    internal fun observeProgressState(progressStateSubject: PublishSubject<BaseViewModel.ProgressState>) {
        progressStateSubject
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { state ->
                    when (state) {
                        BaseViewModel.ProgressState.Show -> {
                            onShowProgress()
                        }

                        BaseViewModel.ProgressState.Hide -> {
                            progressView?.hide()
                            onHideProgress()
                        }
                    }
                },

                onError = { Timber.e("$it @ BaseFragment::progressStateSubject") }
            ).addTo(disposable)
    }

    fun @receiver:ColorRes Int.toColor(): Int {
        return this.toColor(requireContext())
    }

    fun @receiver:DimenRes Int.toDimen(): Float {
        return this.toDimen(requireContext())
    }

    fun @receiver:DrawableRes Int.toDrawable(): Drawable? {
        return this.toDrawable(requireContext())
    }
}
