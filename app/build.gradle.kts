import Dependencies.Module.data
import com.android.build.gradle.internal.dsl.BuildType
import com.android.build.gradle.internal.dsl.DefaultConfig

plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-kapt")
    id("dagger.hilt.android.plugin")
}

android {
    compileSdkVersion(AndroidSDK.compile)
    buildToolsVersion = AndroidSDK.buildToolsVersion

    defaultConfig {
        applicationId = ProjectConfig.applicationId
        minSdkVersion(AndroidSDK.min)
        targetSdkVersion(AndroidSDK.target)
        versionCode = ProjectConfig.versionCode
        versionName = ProjectConfig.versionName

        multiDexEnabled = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        stringField(
            Field.VITRINOVA_SERVICE_URL to "https://www.vitrinova.com/"
        )
    }

    buildTypes {
        getByName("debug") {
            isMinifyEnabled = false
        }

        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    // Enables Android ViewBinding
    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar", "*.aar"))))
    implementation(Dependencies.Kotlin.kotlinStdLib)
    implementation(data())

    // Android
    implementation(Dependencies.Android.kotlinCoroutinesAndroid)
    implementation(Dependencies.Android.androidCore)
    implementation(Dependencies.Android.appCompat)
    implementation(Dependencies.Android.legacySupport)
    implementation(Dependencies.Android.fragment)
    implementation("androidx.legacy:legacy-support-v4:1.0.0")
    implementation(Dependencies.Android.multidex)
    implementation(Dependencies.Android.material)
    // ---- LifeCycle
    implementation(Dependencies.Android.constraintLayout)
    implementation(Dependencies.Android.recyclerView)
    implementation(Dependencies.Android.viewPager2)
    implementation(Dependencies.Android.LifeCycle.extensions)
    implementation(Dependencies.Android.LifeCycle.viewModel)
    implementation(Dependencies.Android.LifeCycle.hiltLifecycleViewModel)
    kapt(Dependencies.Android.LifeCycle.hiltCompiler)

    // Test
    testImplementation(Dependencies.Test.junit)
    testImplementation(Dependencies.Test.kotlinJunit)
    testImplementation(Dependencies.Test.kluent)
    androidTestImplementation(Dependencies.Android.Test.espressoCore)
    androidTestImplementation(Dependencies.Android.Test.androidJunit)
    debugImplementation(Dependencies.Android.Test.fragmentTesting)

    // Reactive Extensions
    implementation(Dependencies.ReactiveExtensions.rxJava)
    implementation(Dependencies.ReactiveExtensions.rxKotlin)
    implementation(Dependencies.ReactiveExtensions.rxAndroid)

    // Dagger
    implementation(Dependencies.Dagger.dagger)
    implementation(Dependencies.Dagger.daggerHilt)
    kapt(Dependencies.Dagger.daggerAnnotationProcessor)
    kapt(Dependencies.Dagger.daggerHiltAnnotationProcessor)
    androidTestImplementation(Dependencies.Dagger.Test.daggerHiltTesting)

    // Timber
    implementation(Dependencies.timber)

    // Calligraphy
    implementation(Dependencies.calligraphy)
    implementation(Dependencies.viewpump)

    // Image
    implementation(Dependencies.glide)
    implementation(Dependencies.glideOkHttpIntegration)
    kapt(Dependencies.glideCompiler)

    // Input Mask
    implementation(Dependencies.inputMask)

    // Lottie (Animation)
    implementation(Dependencies.lottie)

    // LeakCanary
    debugImplementation(Dependencies.leakCanary)

    // ViewBinding by delegate (link: https://github.com/kirich1409/ViewBindingPropertyDelegate )
//    implementation(Dependencies.viewBindingDelegate)
    implementation(Dependencies.viewBindingDelegateNoReflection)
}

kapt {
    correctErrorTypes = true
}

fun BuildType.stringField(entry: Pair<String, String>) {
    buildConfigField("String", entry.first, "\"${entry.second}\"")
}

fun DefaultConfig.stringField(entry: Pair<String, String>) {
    buildConfigField("String", entry.first, "\"${entry.second}\"")
}

fun BuildType.booleanField(entry: Pair<String, Boolean>) {
    buildConfigField("Boolean", entry.first, "${entry.second}")
}

fun DefaultConfig.booleanField(entry: Pair<String, Boolean>) {
    buildConfigField("Boolean", entry.first, "${entry.second}")
}

fun BuildType.stringRes(entry: Pair<String, String>) {
    resValue("string", entry.first, entry.second)
}

fun DefaultConfig.stringRes(entry: Pair<String, String>) {
    resValue("string", entry.first, entry.second)
}

fun com.android.build.gradle.internal.dsl.ProductFlavor.stringField(entry: Pair<String, String>) {
    buildConfigField("String", entry.first, "\"${entry.second}\"")
}

fun com.android.build.gradle.internal.dsl.ProductFlavor.booleanField(entry: Pair<String, Boolean>) {
    buildConfigField("Boolean", entry.first, "${entry.second}")
}
