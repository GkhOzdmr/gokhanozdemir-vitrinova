package dev.gokhanozdemir.data.entity

import com.google.gson.annotations.SerializedName

data class ShopEntity(
    @SerializedName("id") val id: Int?,
    @SerializedName("name") val name: String?,
    @SerializedName("slug") val slug: String?,
    @SerializedName("definition") val definition: String?,
    @SerializedName("name_updateable") val nameUpdateable: Boolean?,
    @SerializedName("vacation_mode") val vacationMode: Int?,
    @SerializedName("created_at") val createdAt: String?,
    @SerializedName("shop_payment_id") val shopPaymentId: Int?,
    @SerializedName("product_count") val productCount: Int?,
    @SerializedName("shop_rate") val shopRate: Int?,
    @SerializedName("comment_count") val commentCount: Int?,
    @SerializedName("follower_count") val followerCount: Int?,
    @SerializedName("is_editor_choice") val isEditorChoice: Boolean?,
    @SerializedName("is_following") val isFollowing: Boolean?,
    @SerializedName("cover") val cover: CoverEntity?,
    @SerializedName("share_url") val shareUrl: String?,
    @SerializedName("logo") val logo: LogoEntity?
)