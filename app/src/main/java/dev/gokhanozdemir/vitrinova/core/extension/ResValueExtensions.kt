package dev.gokhanozdemir.vitrinova.core.extension

import android.content.Context
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat


/**
 * Retrieves color from colors.xml with given [androidx.annotation.ColorRes]
 */
fun @receiver:ColorRes Int.toColor(context: Context): Int {
    return ResourcesCompat.getColor(context.resources, this, null)
}

fun @receiver:DimenRes Int.toDimen(context: Context) = context.resources.getDimension(this)

fun @receiver:DrawableRes Int.toDrawable(context: Context) =
    ContextCompat.getDrawable(context, this)

